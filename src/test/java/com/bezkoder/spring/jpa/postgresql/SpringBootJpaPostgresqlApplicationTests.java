package com.bezkoder.spring.jpa.postgresql;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class SpringBootJpaPostgresqlApplicationTests {

	@Test
	public void exampleTest() {
		String str1 = "testcase";
		assertEquals("testcase", str1);

	}

}
