resource "aws_autoscaling_policy" "asg_scaling_policy_1" {
  name                   = "ASG_scaling_policy_1"
  autoscaling_group_name = aws_autoscaling_group.web-asg-1.name

  policy_type = "TargetTrackingScaling"
  estimated_instance_warmup = 300
 
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = "50"  
  }
}

resource "aws_cloudwatch_metric_alarm" "cw_metric_alarm" {
  alarm_name          = "CPU_Alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "60"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web-asg-1.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [ aws_sns_topic.app-topic.arn ]
}

resource "aws_cloudwatch_metric_alarm" "instance-health-check" {
  alarm_name                = "instance-health-check"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "StatusCheckFailed"
  namespace                 = "AWS/EC2"
  period                    = "60"
  statistic                 = "Average"
  threshold                 = "1"
  alarm_description         = "This metric monitors ec2 health status"
  alarm_actions             = [ aws_sns_topic.app-topic.arn ]
  
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web-asg-1.name
  }
}