variable "region" {  
  type        = string  
  default     = "us-east-1"  
  description = "Default aws region"
}

variable "availability_zone_names" {
  type    = list(string)
  default = ["us-east-1a",
        "us-east-1b"
            ]
}

variable "image_id" {  
  type        = string  
  default     = "ami-0a4b6e4124ff0fc01"
  #default     = "ami-red"  
  description = "The image for the deployment"
}

variable "ec2_name" {  
  type        = string  
  #default    = "ami-0a4b6e4124ff0fc01"
  default     = "spring-boot"  
  description = "The ec2_name"
}

variable "desired_capacity_1" {  
  type        = number  
  default     = 2
  description = "The desired_capacity"
}

variable "max_size_1" {  
  type        = number  
  default     = 2
  description = "The max_size"
}

variable "min_size_1" {  
  type        = number  
  default     = 1
  description = "The min_size"
}

variable "desired_capacity_2" {  
  type        = number  
  default     = 0
  description = "The desired_capacity"
}

variable "max_size_2" {  
  type        = number  
  default     = 0
  description = "The max_size"
}

variable "min_size_2" {  
  type        = number  
  default     = 0
  description = "The min_size"
}

variable "release-to-asg-1" {  
  type        = bool 
  default     = false
  description = "Whether the release is made to ASG set 1 or set 2"
}

variable "release-complete-asg-1" {  
  type        = bool 
  default     = false
  description = "Whether the release is made to ASG set 1 or set 2"
}

variable "app-topic-subscriber-email" {  
  type        = string 
  description = "Email subscriber"
}

# Defining CIDR Block for VPC
variable "vpc_cidr" {
  default = "20.0.0.0/16"
}
# Defining CIDR Block for 1st Subnet
variable "subnet_cidr" {
  default = "20.0.1.0/24"
}
# Defining CIDR Block for 2nd Subnet
variable "subnet1_cidr" {
  default = "20.0.2.0/24"
}
# Defining CIDR Block for 3rd Subnet
variable "subnet2_cidr" {
  default = "20.0.3.0/24"
}
# Defining CIDR Block for 4th Subnet
variable "subnet3_cidr" {
  default = "20.0.4.0/24"
}
# Defining CIDR Block for 5th Subnet
variable "subnet4_cidr" {
  default = "20.0.5.0/24"
}
# Defining CIDR Block for 6th Subnet
variable "subnet5_cidr" {
  default = "20.0.6.0/24"
}


# Defining Elastic OpenSearch variables
variable "es_domain" {
  type = string
}

variable "es_instance_type" {
  type = string
}

variable "es_volume_type" {
  type = string
}

variable "es_ebs_volume_size" {
  type = number
}

variable "cwl_endpoint" {
  type = string
  default = "logs.us-east-1.amazonaws.com"
}

variable "create_es_service_linked_role" {
  type        = bool
  default     = true
  description = "Whether to create `AWSServiceRoleForAmazonElasticsearchService` service-linked role. Set it to `false` if you already have an ElasticSearch cluster created in the AWS account and AWSServiceRoleForAmazonElasticsearchService already exists. See https://github.com/terraform-providers/terraform-provider-aws/issues/5218 for more info"
}
