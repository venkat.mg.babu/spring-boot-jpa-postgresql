data "aws_ssm_parameter" "springboot_latest_code_mage" {
    name = "/springboot/lates-code-ami"
}

resource "aws_launch_template" "web-launch-template" {
  name_prefix           = "${var.ec2_name}-${terraform.workspace}"
  image_id              = "${data.aws_ssm_parameter.springboot_latest_code_mage.value}"
  instance_type         = "t2.micro"

  key_name              = "MyKeyPair"
  
  vpc_security_group_ids = ["${aws_security_group.demosg.id}"]

  #network_interfaces {
  #  associate_public_ip_address = true
  #  security_groups      = ["${aws_security_group.demosg.id}"]    
  #}

  monitoring {
    enabled = true
  }

  iam_instance_profile  {
      arn = aws_iam_instance_profile.ec2_instance_profile.arn
  }

  user_data             = filebase64("userdata.sh")

  tags = {
    env = "${terraform.workspace}"
  }
}


resource "aws_autoscaling_group" "web-asg-1" {
  #availability_zones = ["us-east-1a", "us-east-1b"]

  # count = var.release-to-asg-1 ? 1 : 0

  vpc_zone_identifier = ["${aws_subnet.public-subnet-1.id}", "${aws_subnet.public-subnet-2.id}" ]
  desired_capacity   = "${var.desired_capacity_1}"
  max_size           = "${var.max_size_1}"
  min_size           = "${var.min_size_1}"
  
  launch_template {
    id      = aws_launch_template.web-launch-template.id
    version = "$Latest"
  }

  target_group_arns        = [aws_lb_target_group.alb-target-group-1.arn]
}


resource "aws_autoscaling_group" "web-asg-2" {
  #availability_zones = ["us-east-1a", "us-east-1b"]
  vpc_zone_identifier = ["${aws_subnet.public-subnet-1.id}", "${aws_subnet.public-subnet-2.id}" ]
  desired_capacity   = "${var.desired_capacity_2}"
  max_size           = "${var.max_size_2}"
  min_size           = "${var.min_size_2}"
  
  launch_template {
    id      = aws_launch_template.web-launch-template.id
    version = "$Latest"
  }

  target_group_arns        = [aws_lb_target_group.alb-target-group-2.arn]
}
