# Creating RDS Instance
resource "aws_db_subnet_group" "default" {
  name       = "main"
  subnet_ids = [aws_subnet.database-subnet-1.id, aws_subnet.database-subnet-2.id]

  tags = {
    Name = "My DB subnet group"
  }
}

data "aws_db_snapshot" "latest_dev_snapshot" {
  db_instance_identifier = "test-pg-1"
  most_recent            = true
}

resource "aws_db_instance" "dev" {

  snapshot_identifier    =  "${data.aws_db_snapshot.latest_dev_snapshot.id}"
  allocated_storage      = 20
  db_subnet_group_name   = aws_db_subnet_group.default.id
  engine                 = "postgres"
  engine_version         = "13.7"
  instance_class         = "db.t3.micro"
  multi_az               = false
  #db_name always goes for replacement
  #db_name                = "test-pg-1"
  identifier             = "test-pg-1"
  username               = "postgres"
  password               = "postgres"
  
  storage_encrypted      = true
  vpc_security_group_ids = [aws_security_group.database-sg.id]
  publicly_accessible    = true
  # monitoring_interval    = 60

  skip_final_snapshot       = false
  final_snapshot_identifier = "test-pg-snapshot-${random_uuid.sb_uuid.result}"

  lifecycle {
    ignore_changes = [snapshot_identifier]
  }

  depends_on = [
    aws_vpc.demovpc,
    aws_subnet.database-subnet-1,
    aws_internet_gateway.demogateway,
    aws_route_table.route,
    aws_security_group.database-sg,
    aws_lb.external-alb-1 
  ]

}

