resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "aws_instance_access_profile"
  role = aws_iam_role.aws_instance_role.name
}

resource "aws_iam_role" "aws_instance_role" {
  name = "tf_role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "ec2_policy" {
  name        = "ec2_policy"
  description = "A EC2 to access s3, cloudwatch logs policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ec2_instance_attach" {
  role       = aws_iam_role.aws_instance_role.name
  policy_arn = aws_iam_policy.ec2_policy.arn
}
