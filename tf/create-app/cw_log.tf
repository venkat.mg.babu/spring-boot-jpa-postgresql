resource "aws_cloudwatch_log_group" "cw_log_group" {
  name = "spring-boot-logging"
  retention_in_days = 14

  tags = {
    environment = "dev"
    application = "spring-boot"
  }
}