resource "aws_iam_role" "lambda_elasticsearch_execution_role" {
  name = "lambda_elasticsearch_execution_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lambda_elasticsearch_execution_policy" {
  name = "lambda_elasticsearch_execution_policy"
  role = "${aws_iam_role.lambda_elasticsearch_execution_role.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:*",
        "ec2:*"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": "es:ESHttpPost",
      "Resource": "arn:aws:es:*:*:*"
    }
  ]
}
EOF
}

resource "aws_lambda_function" "cwl_stream_lambda" {
  filename         = "es_cwl2eslambda.zip"
  function_name    = "LogsToElasticsearch"
  role             = "${aws_iam_role.lambda_elasticsearch_execution_role.arn}"
  handler          = "es_cwl2eslambda.handler"
  source_code_hash = filebase64sha256("es_cwl2eslambda.zip")
  runtime          = "nodejs16.x"

  vpc_config {
          subnet_ids = [
                aws_subnet.public-subnet-1.id,      
    ]
    security_group_ids = [aws_security_group.es_sg.id]
   }

  environment {
    variables = {
      es_endpoint = "${aws_elasticsearch_domain.es.endpoint}"
    }
  }
}

resource "aws_lambda_permission" "cloudwatch_allow" {
  statement_id = "cloudwatch_allow"
  action = "lambda:InvokeFunction"
  # function_name = "${aws_lambda_function.cwl_stream_lambda.arn}"
  # principal = "${var.cwl_endpoint}"
  # source_arn = "${aws_cloudwatch_log_group.cw_log_group.arn}"

  function_name   = aws_lambda_function.cwl_stream_lambda.function_name
  principal       = "${var.cwl_endpoint}"
  source_arn      = format("%s:*", aws_cloudwatch_log_group.cw_log_group.arn)

}

resource "aws_cloudwatch_log_subscription_filter" "cloudwatch_logs_to_es" {
  depends_on = [aws_lambda_permission.cloudwatch_allow]
  name            = "cloudwatch_logs_to_elasticsearch"
  log_group_name  = "${aws_cloudwatch_log_group.cw_log_group.name}"
  filter_pattern  = ""
  destination_arn = "${aws_lambda_function.cwl_stream_lambda.arn}"
}
