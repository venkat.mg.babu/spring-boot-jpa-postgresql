image_id = "ami-0da997662ec558272"


availability_zone_names = [
  "us-east-1a",
  "us-west-1b",
]

region = "us-east-1"

min_size_1          = 1
desired_capacity_1  = 1
max_size_1          = 2

min_size_2          = 0
desired_capacity_2  = 0
max_size_2          = 0

release-to-asg-1 = true

app-topic-subscriber-email = "mgvenkateshbabu.1512@gmail.com"

# Defining CIDR Block for VPC
vpc_cidr = "20.0.0.0/16"

# Defining CIDR Block for 1st Subnet
subnet_cidr= "20.0.1.0/24"

# Defining CIDR Block for 2nd Subnet
subnet1_cidr = "20.0.2.0/24"

# Defining CIDR Block for 3rd Subnet
subnet2_cidr = "20.0.3.0/24"

# Defining CIDR Block for 4th Subnet
subnet3_cidr = "20.0.4.0/24"

# Defining CIDR Block for 5th Subnet
subnet4_cidr = "20.0.5.0/24"

# Defining CIDR Block for 6th Subnet
subnet5_cidr = "20.0.6.0/24"

# Defining Elastic OpenSearch variables
es_domain        = "esdomain"

es_instance_type = "r5.large.elasticsearch"
# es_instance_type = "t3.small.elasticsearch"

es_volume_type   = "gp2"

es_ebs_volume_size  = 10

cwl_endpoint = "logs.us-east-1.amazonaws.com"

create_es_service_linked_role = true