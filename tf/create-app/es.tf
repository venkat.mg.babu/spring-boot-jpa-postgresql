data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

resource "aws_security_group" "es_sg" {
  name        = "${aws_vpc.demovpc.id}-elasticsearch-${var.es_domain}"
  description = "Managed by Terraform"
  vpc_id      = "${aws_vpc.demovpc.id}"

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound Rules
  # Internet access to anywhere
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_iam_service_linked_role" "es_service_linked_role" {
  count            = "${var.create_es_service_linked_role ? 1 :0 }" 
  aws_service_name = "es.amazonaws.com"
  description      = "Allows Amazon ES to manage AWS resources for a domain on your behalf."
}

resource "aws_elasticsearch_domain" "es" {
  domain_name           = var.es_domain
  elasticsearch_version = "OpenSearch_2.3"

  cluster_config {
    instance_type          = var.es_instance_type
  }

  ebs_options {
    ebs_enabled = var.es_ebs_volume_size > 0 ? true : false
    volume_size = var.es_ebs_volume_size
    volume_type = var.es_volume_type
  }

  vpc_options {
    subnet_ids = [
      aws_subnet.public-subnet-1.id,      
    ]

    security_group_ids = [aws_security_group.es_sg.id]
  }

  # advanced_options = {
  #   "rest.action.multi.allow_explicit_index" = "true"
  # }

  access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": "*",
            "Effect": "Allow",
            "Resource": "arn:aws:es:${var.region}:${data.aws_caller_identity.current.account_id}:domain/${var.es_domain}/*"
        }
    ]
}
CONFIG

  tags = {
    Domain = "SpringBootDomain"
  }

  depends_on = [aws_iam_service_linked_role.es_service_linked_role]
}