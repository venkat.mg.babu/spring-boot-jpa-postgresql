# Getting the DNS of load balancer
output "lb_dns_name-1" {
  description = "The DNS name of the load balancer-1"
  value       = aws_lb.external-alb-1.dns_name
}

# Getting the DNS of load balancer
output "lb_dns_name-2" {
  description = "The DNS name of the load balancer-2"
  value       = aws_lb.external-alb-2.dns_name
}

# Getting the ASG for set-1
output "ASG-1" {
  description = "The ASG name for set 1"
  value       = aws_autoscaling_group.web-asg-1.name
}

# Getting the ASG for set-2
output "ASG-2" {
  description = "The ASG name for set 2"
  value       = aws_autoscaling_group.web-asg-2.name
}

# Defining Elastic OpenSearch outputs
output "es_arn" {
    value = aws_elasticsearch_domain.es.arn
} 
output "es_domain_id" {
    value = aws_elasticsearch_domain.es.domain_id
} 

output "es_endpoint" {
    value = aws_elasticsearch_domain.es.endpoint
} 
output "es_kibana_endpoint" {
    value = aws_elasticsearch_domain.es.kibana_endpoint
}