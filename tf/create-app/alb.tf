# Creating External LoadBalancer
resource "aws_lb" "external-alb-1" {
  name               = "External-ALB-1"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.demosg.id]
  subnets            = [aws_subnet.public-subnet-1.id, aws_subnet.public-subnet-2.id]
}

# Creating External LoadBalancer
resource "aws_lb" "external-alb-2" {
  name               = "External-ALB-2"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.demosg.id]
  subnets            = [aws_subnet.public-subnet-1.id, aws_subnet.public-subnet-2.id]
}

resource "aws_lb_target_group" "alb-target-group-1" {
  name     = "ALB-TG-1"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = aws_vpc.demovpc.id

  health_check {
    path                = "/api/tutorials"
    port                = 8080
    protocol            = "HTTP"
    healthy_threshold   = 3
    unhealthy_threshold = 3
    matcher             = "200"
  }
}

resource "aws_lb_target_group" "alb-target-group-2" {
  name     = "ALB-TG-2"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = aws_vpc.demovpc.id

  health_check {
    path                = "/api/tutorials"
    port                = 8080
    protocol            = "HTTP"
    healthy_threshold   = 3
    unhealthy_threshold = 3
    matcher             = "200"
  }
}

resource "aws_lb_listener" "external-elb-listener-for-asg-1" {
  load_balancer_arn = aws_lb.external-alb-1.arn
  port              = "8080"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb-target-group-1.arn
  }
}

resource "aws_lb_listener_rule" "external-elb-listener-for-asg-1-fixed-response" {
  count = "${var.release-to-asg-1}" ? 0 : 1
  listener_arn = aws_lb_listener.external-elb-listener-for-asg-1.arn

  action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Spring boot applicationPage not found"
      status_code  = "404"
    }
  }

 condition {
    path_pattern {
      values = ["/api/*"]
    }
  }
}

resource "aws_lb_listener" "external-elb-listener-for-asg-2" {
  load_balancer_arn = aws_lb.external-alb-2.arn
  port              = "8080"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb-target-group-2.arn
  }
}

resource "aws_lb_listener_rule" "external-elb-listener-for-asg-2-fixed-response" {
  count = var.release-to-asg-1 ? 1 : 0
  listener_arn = aws_lb_listener.external-elb-listener-for-asg-2.arn

  action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Spring boot applicationPage not found"
      status_code  = "404"
    }
  }

  condition {
    path_pattern {
      values = ["/api/*"]
    }
  }
}