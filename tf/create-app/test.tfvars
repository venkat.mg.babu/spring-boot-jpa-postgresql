image_id = "ami-0a4b6e4124ff0fc01"

availability_zone_names = [
  "us-east-1c",
  "us-west-1d",
]

region = "us-east-1"

# Defining CIDR Block for VPC
vpc_cidr = "30.0.0.0/16"

# Defining CIDR Block for 1st Subnet
subnet_cidr= "30.0.1.0/24"

# Defining CIDR Block for 2nd Subnet
subnet1_cidr = "30.0.2.0/24"

# Defining CIDR Block for 3rd Subnet
subnet2_cidr = "30.0.3.0/24"

# Defining CIDR Block for 4th Subnet
subnet3_cidr = "30.0.4.0/24"

# Defining CIDR Block for 5th Subnet
subnet4_cidr = "30.0.5.0/24"

# Defining CIDR Block for 6th Subnet
subnet5_cidr = "30.0.6.0/24"
