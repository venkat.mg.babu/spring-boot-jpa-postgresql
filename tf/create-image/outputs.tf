# Getting the DNS of load balancer
output "ec2_id" {
  description = "The arn resource of EC2"
  value       = aws_instance.ec2_for_create_latest_code_image[0].id
}