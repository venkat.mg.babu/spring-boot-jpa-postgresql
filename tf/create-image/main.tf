data "aws_ssm_parameter" "springboot_base_mage" {
    name = "/springboot/base-ami"
}

# Creating 1st EC2 instance in Public Subnet
resource "aws_instance" "ec2_for_create_latest_code_image" {
  ami                         = "${data.aws_ssm_parameter.springboot_base_mage.value}"
  instance_type               = "t2.micro"
  count                       = 1
  key_name                    = "MyKeyPair"
  associate_public_ip_address = true
  # user_data                   = "${file("data.sh")}"
  user_data                   = "${file("userdata.sh")}"

  iam_instance_profile        = aws_iam_instance_profile.ec2_instance_profile.name

  tags = {
    app = "spring-boot-app"
    env = "dev"
    name = "create-latest-code-ami"
  }

}

resource "aws_ssm_parameter" "cw_agent_config_data" {
  description = "Cloudwatch agent config to configure custom log"
  name        = "/cloudwatch-agent/config"
  type        = "String"
  value       = file("cw_agent_config.json")
}


locals {
  userdata = templatefile("userdata.sh", {
    ssm_cloudwatch_config = aws_ssm_parameter.cw_agent_config_data.name
  })
}