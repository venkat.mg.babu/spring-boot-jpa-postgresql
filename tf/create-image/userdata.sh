#!/bin/bash

#echo "hello world" > /home/ec2-user/sample_text.txt
#aws ec2 describe-instances --region "us-east-1" > /home/ec2-user/ec2-instances.txt

aws s3 cp s3://website.example.com-909090/spring-boot-jpa-postgresql-3.jar /home/ec2-user/springboot --region "us-east-1"

/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl \
    -a fetch-config \
    -m ec2 \
    -c ssm:${ssm_cloudwatch_config} -s

sudo systemctl enable amazon-cloudwatch-agent