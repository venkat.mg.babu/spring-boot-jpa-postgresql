variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}


variable "env_name" {  
  type        = string  
  default     = "dev"  
  description = "Environment"
}

variable "prometheus_namespace" {  
  type        = string  
  default     = "prometheus-ns"  
  description = "prometheus_namespace"
}

variable "grafana_namespace" {  
  type        = string  
  default     = "grafana-ns"  
  description = "grafana_namespace"
}

variable "monitoring_namespace" {  
  type        = string  
  default     = "monitoring-ns"  
  description = "monitoring_ns"
}

variable "kube_prometheus_version" {  
  type        = string  
  default     = "36.2.0"  
  description = "monitoring-ns"
}

# Defining Elastic OpenSearch variables
variable "es_domain" {
  type = string
  default = "esdomain"
}

variable "es_instance_type" {
  type = string
  default = "t3.small.elasticsearch"
}

variable "es_volume_type" {
  type = string
  default = "gp2"
}

variable "es_ebs_volume_size" {
  type = number
  default = 10
}

variable "cwl_endpoint" {
  type = string
  default = "logs.us-east-1.amazonaws.com"
}

variable "create_es_service_linked_role" {
  type        = bool
  default     = true
  description = "Whether to create `AWSServiceRoleForAmazonElasticsearchService` service-linked role. Set it to `false` if you already have an ElasticSearch cluster created in the AWS account and AWSServiceRoleForAmazonElasticsearchService already exists. See https://github.com/terraform-providers/terraform-provider-aws/issues/5218 for more info"
}