module "argocd_addons" {

  source = "github.com/aws-ia/terraform-aws-eks-blueprints//modules/kubernetes-addons?ref=v4.0.2"
  
  enable_argocd                       = true
#   argocd_admin_password_secret_name   = "$2a$12$P0ewaMScCRnRKxu.8otuZelRIm.4v1j1F9oGxBiq2RGZnDfCs70hS"
  eks_cluster_id = module.eks.cluster_name

  argocd_helm_config = {
    name             = "argo-cd"
    chart            = "argo-cd"
    repository       = "https://argoproj.github.io/argo-helm"
    version          = "3.29.5"
    namespace        = "argocd"
    timeout          = "1200"
    create_namespace = true
    values = [templatefile("${path.module}/argocd-values.yaml", {})]
  }

  argocd_applications = {
    workloads = {
    path                = "envs/dev"
    repo_url            = "https://github.com/aws-samples/eks-blueprints-workloads.git"
    values              = {}
  }
 
}

}