provider "aws" {
    region     = var.region
    shared_credentials_files = ["~/.aws/credentials"]
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.45.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~> 3.1.0"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "~> 3.4.0"
    }

    cloudinit = {
      source  = "hashicorp/cloudinit"
      version = "~> 2.2.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.12.1"
    }
  }

  required_version = "~> 1.2.9"

  backend "s3" {
    bucket         = "s3-tf-backend-0909090"
    key            = "my-eks-cluster/helm/terraform.tfstate"
    region         = "us-east-1"
    #encrypt        = true
    #kms_key_id     = "arn:aws:kms:us-east-1:863992639968:key/92b8e392-74ad-4cef-8985-d53f83b2bbcc"
    dynamodb_table = "db-tf-backend-1"
  }
}

