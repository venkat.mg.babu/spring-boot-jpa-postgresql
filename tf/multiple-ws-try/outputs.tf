# Getting the DNS of load balancer
output "ec2_id" {
  description = "The arn resource of EC2"
  value       = aws_instance.demoinstance[0].id
}