# Creating 1st EC2 instance in Public Subnet
resource "aws_instance" "demoinstance" {
  ami                         = "ami-0b7bd0575e636746f"
  instance_type               = "${var.instance_type}"
  count                       = 1
  key_name                    = "MyKeyPair"
  associate_public_ip_address = true
  
  tags = {
    Name = "spring-boot-${terraform.workspace}"
  }

}