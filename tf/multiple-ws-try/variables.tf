variable "region" {  
  type        = string  
  default     = "us-east-1"  
  description = "Default aws region"
}

variable "instance_type" {  
  type        = string  
  default     = "t2.micro"  
  description = "Default aws instance"
}


